#Repository for the CHPC conference workshop on Tensorflow
- Opening the PDFs may be useful as you work through the tasks
- The solutions folder contains complete code. Try not to read it before attempting the tasks.
- The Template folder contains barbones code to get you started. Don't expect too much though.
- Please add the contents of bashrc_edit to ~/.bashrc when you login to the GPU node