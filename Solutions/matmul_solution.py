'''
A simple matrix multiplication task

Author: Jarryd Son
'''

import tensorflow as tf

#Build the computational graph
#===============================

a = tf.constant([[3. ,3.]])
b = tf.constant([[2.] ,[2.]])

product = tf.matmul(a,b)

#Run the graph in a session
#===============================

with tf.Session() as sess:
    result = sess.run(product)
    print ("Result: {0}".format(result))