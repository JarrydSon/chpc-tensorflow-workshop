'''
Learning to build and run the computational graph
Author: Jarryd Son

'''

import tensorflow as tf

#Build the computational graph
#===============================

a = tf.constant(5)
b = tf.constant(3)

sum = tf.add(a,b)

#Run the graph in a session
#===============================

with tf.Session() as sess:
    result = sess.run(sum)
    print ("Result: {0}".format(result))