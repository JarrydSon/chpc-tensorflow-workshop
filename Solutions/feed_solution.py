'''
Using placeholders to feed a list of matrices to be multiplied

Author: Jarryd Son
'''

import tensorflow as tf

# List of input matrics just two 
# 3 x 3 matrices per input to keep it simple
#===========================================
input1 = [[[1, 2, 3], [1, 2, 3], [0, 0, 0]],
          [[3, 2, 1], [3, 2, 1], [0, 0, 0]]]
input2 = [[[1, 2, 3], [1, 2, 3], [0, 0, 0]],
          [[3, 2, 1], [3, 2, 1], [0, 0, 0]]]

# Define placeholders
#===========================================
A = tf.placeholder(tf.float32, [3 ,3]) #define the type and shape of the expected input
B = tf.placeholder(tf.float32, [3 ,3])

# Build the computational graph
#==========================================
product = tf.matmul(A ,B)

# Run the graph in a session
#==========================================
with tf.Session() as sess:
    for i in range(2):
        result = sess.run([product], feed_dict={A: input1[i], B: input2[i]})
        print ("Result:\n{0}".format(result[0])) #result is a numpy array