'''
A counter executed in a graph

Author: Jarryd Son
'''

import tensorflow as tf

# Define state variable
#===============================
state = tf.Variable(0)

# Build computational graph
#===============================
one = tf.constant(1)
new_value = tf.add(state, one)
update = tf.assign(state, new_value)

# Initialise variables op
#===============================
init_op = tf.global_variables_initializer()

# Run graph in a session
#===============================
with tf.Session() as sess:
    sess.run(init_op)
    print ("Current state: {0}".format(sess.run(state)))
    for _ in range(3):
        sess.run(update)
        print ("Current state: {0}".format(sess.run(state)))
        
        