'''
A Multilayer Perceptron implementation example using TensorFlow library.
This example is using the MNIST database of handwritten digits
(http://yann.lecun.com/exdb/mnist/)

Words in all UPPERCASE are instructions to be carried out

Author: Jarryd Son - modified from Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
'''

from __future__ import print_function

# Import mnist data
#============================================================
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

import tensorflow as tf

# Parameters (best to stick to these)
#============================================================
learning_rate = 0.001
training_epochs = 15
batch_size = 100
display_step = 1

# Network Parameters
#============================================================
n_hidden_1 = 256 # 1st layer number of features
n_hidden_2 = 256 # 2nd layer number of features
n_input = 784 # mnist data input (img shape: 28*28)
n_classes = 10 # mnist total classes (0-9 digits)

# Define placeholders for the inputs to the graph
# INSERT code to create appropriate placeholders 
# for the training images and labels.
# Hint: One can use the "None" to indicate a variable dimension 
#============================================================



# Define variables to store weights and biases
# A dictionary is used just for compactness
# ENTER in the correct sizes for the variables
#============================================================

weights = {
    'h1': tf.Variable(tf.random_normal([])),
    'h2': tf.Variable(tf.random_normal([])),
    'out': tf.Variable(tf.random_normal([]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([])),
    'b2': tf.Variable(tf.random_normal([])),
    'out': tf.Variable(tf.random_normal([]))
}

# Create model
#============================================================
def multilayer_perceptron(input, weights, biases):
    # INSERT necessary ops to create the hidden layers 
    
    
    # Keep the output layer as it is
    out_layer = tf.add(tf.matmul(<insert_argument>, weights['out']),biases['out'])
    return out_layer

# Construct model (determine predicted output)
# MODIFY as necessary
#============================================================
pred = multilayer_perceptron(<insert_argument> , weights, biases)

# Define loss and optimizer
# ENTER your arguments where necessary
#============================================================
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(<predicted_label>, <actual_label>))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Launch the graph
#============================================================
with tf.Session() as sess:

    # Training cycle
    for epoch in range(training_epochs):
        avg_cost = 0.
        total_batch = int(mnist.train.num_examples/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_x, batch_y = mnist.train.next_batch(batch_size)
            
            # Run optimization op (backprop) and cost op (to get loss value)
            # MODIFY the following line as necessary
            _, c = sess.run()
            
            # Compute average loss
            avg_cost += c / total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", \
                "{:.9f}".format(avg_cost))
    print("Optimization Finished!")

    # Testing phase:
    # Has the model generalized to recognize images it has never seen before 
    # mnist.test.images returns all test images
    # mnist.test.labels returns all test labels
    #============================================================
    
    # Define op to determine if a prediction is correct
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    # Define op to determine the accuracy of the predictions 
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    
    # INSERT code to execute the testing phase
    #============================================================
    
    
    
    