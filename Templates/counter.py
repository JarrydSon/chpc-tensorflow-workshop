'''
A counter executed in a graph

Author: Jarryd Son
'''

import tensorflow as tf

# Define state variable
#===============================

# Build computational graph
#===============================

# Initialise variables op
#===============================

# Run graph in a session
#===============================
with tf.Session() as sess:
    
    print ("Current state: {0}".format(sess.run(state)))
    
        
        