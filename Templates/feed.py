'''
Feeding in two sets of matrices into a graph using placeholders

Author: Jarryd Son
'''

import tensorflow as tf

# List of input matrics just two 
# 3 x 3 matrices per input to keep it simple
#===========================================
input1 = [[[1, 2, 3], [1, 2, 3], [0, 0, 0]],
          [[3, 2, 1], [3, 2, 1], [0, 0, 0]]]
input2 = [[[1, 2, 3], [1, 2, 3], [0, 0, 0]],
          [[3, 2, 1], [3, 2, 1], [0, 0, 0]]]

# Define placeholders
#===========================================
# INSERT code to create appropriate placeholders

# Build the computational graph
#==========================================
# INSERT necessary ops to the graph

# Run the graph in a session
#==========================================
# INSERT code to execute the graph